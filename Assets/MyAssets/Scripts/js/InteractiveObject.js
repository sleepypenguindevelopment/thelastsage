
public var customSkin : GUISkin = null;

public var player : GameObject;
public var radius : int;
public var startingState : int;
public var states : List.<ObjectState> = new List.<ObjectState>();

private var currentState : int;

private var interacting : boolean = false;
private var inventoryScript : Inventory;
private var clickToMoveScript : Behaviour = null;
private var inGameGuiScript : InGameGui = null;

function Start () {
	currentState = startingState;
	inventoryScript = player.GetComponent(Inventory);
	clickToMoveScript = player.GetComponent("ClickToMove") as Behaviour;
	inGameGuiScript = player.GetComponent(InGameGui);

	InitializeStates();
}

function OnMouseDown() {
	if (!interacting && Vector3.Distance(transform.position, player.transform.position) <= radius + 1) {
		InteractWithObject();
		interacting = false;
	}
}

function InitializeStates() {
	for (var state : ObjectState in states) {
		if (state.gatewayItem != null && state.gatewayItem != "") {
			state.SetGatewayItemPassed(false);
		}
		if (state.gatewayScript != null && state.gatewayScript != "") {
			state.SetGatewayScriptPassed(false);
		}
	}
}

function InteractWithObject() : IEnumerator {
	interacting = true;
	
	if (states[currentState].enterMessage != null && states[currentState].enterMessage != "") {
		ShowMessage(states[currentState].enterMessage, "OK");
	}
	
	// Check whether player has given this object the gatewayItem
	if (!states[currentState].GetGatewayItemPassed()) {
		var activeItem : InventoryItem = inventoryScript.GetActiveItem();
		if (activeItem != null) {			
			if (activeItem.itemName == states[currentState].gatewayItem.itemName) {
				inventoryScript.DeleteFromInventory(activeItem.itemName);
				states[currentState].SetGatewayItemPassed(true);
			}
		}
	}
	
	// If we've passed the gatewayItem and the gatewayScript, perform the exit actions
	if (states[currentState].GetGatewayItemPassed() && states[currentState].GetGatewayScriptPassed()) {
	
		if (states[currentState].exitMessage != null && states[currentState].exitMessage != "") {
			ShowMessage(states[currentState].exitMessage, "OK");
		}
		
		if (states[currentState].exitItem != null) {
			inventoryScript.AddToInventory(states[currentState].exitItem);
			// Should be a bool that determines whether to give once or multiple times
			states[currentState].exitItem = null;
		}
		
		if (states[currentState].exitAnimationTrigger != null && states[currentState].exitAnimationTrigger != "") {
			var animator : Animator = GetComponent("Animator") as Animator;
	        animator.SetTrigger(states[currentState].exitAnimationTrigger);
		}
		if (states[currentState].targetState != -1) {
			var autoSwitchState : boolean = states[currentState].autoSwitchState;
			currentState = states[currentState].targetState;
			if (autoSwitchState) {
				InteractWithObject();
			}
		}
	} else {
		if (states[currentState].failureMessage != null && states[currentState].failureMessage != "") {
			ShowMessage(states[currentState].failureMessage, "OK");
		}
	}
	
}

function ShowMessage(myMessage : String, myTrueButton : String) {
	inGameGuiScript.SendMessage("SetTrueButton", myTrueButton);
	inGameGuiScript.SendMessage("ShowMessage", myMessage);
}

function GetCurrentState() {
	return currentState;
}

function SetCurrentState(newState : int) {
	currentState = newState;
}

class ObjectState {
	public var stateName : String = null;
	
	public var autoInteractRadius : int = -1;
	
	public var enterMessage : String = null;
	
	public var gatewayItem : InventoryItem = null;
	public var gatewayScript : String = null;
	
	public var failureMessage : String = null;
	public var exitMessage : String = null;
	public var exitItem : InventoryItem = null;
	public var exitCallback : String = null;
	public var exitAnimationTrigger : String = null;
	
	public var targetState : int = 0;
	public var autoSwitchState : boolean = false;
	
	private var gatewayItemPassed = true;
	private var gatewayScriptPassed = true;

	function SetGatewayItemPassed(passed : boolean) {
    	gatewayItemPassed = passed;
    }
    
    function GetGatewayItemPassed() : boolean {
    	return gatewayItemPassed;
    }
    
    function SetGatewayScriptPassed(passed : boolean) {
    	gatewayScriptPassed = passed;
    }
    
    function GetGatewayScriptPassed() : boolean {
    	return gatewayScriptPassed;
    }
}