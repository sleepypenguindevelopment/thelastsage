#pragma strict

public var MenuSkin : GUISkin = null;

	public var inGame : boolean = false;

// @TODO: calculate whether to show these or make them public?
	private var enableStartButton : boolean = true;
	private var enableContinueButton : boolean = false;
	private var enableSaveButton : boolean = false;
	private var enableLoadButton : boolean = false;
	private var enableExitButton : boolean = true;
	
	private var screenWidth : float = Screen.width;
	private var screenHeight : float = Screen.height;

	// determine which buttons should display, then calculate menu box height
	private var menuBoxLeft : float;
	private var menuBoxTop : float;
	private var menuBoxWidth : float;
	
	private var buttonLeft : float;
	private var buttonWidth : float;
	private var buttonHeight : float;
	
	private var buttonSpace : float;
	private var buttonTop : float;
	
	private var buttonCount : int;
	
	private var menuBoxHeight : float; 
	
	private var clickToMoveScript : Behaviour = null;
	clickToMoveScript = GetComponent("ClickToMove") as Behaviour;
	
	// @TODO: replace with 2D array of Menu Title and Function to call
	// ex: buttons["startButton"] = {"New Game", "StartNewGame"}

function Start () {
	
	if (clickToMoveScript != null) {
		clickToMoveScript.enabled = false;
	}
	
	if (inGame) {
		menuBoxLeft = screenWidth * 0.20;
		menuBoxTop = screenHeight * 0.20;
		menuBoxWidth = screenWidth * 0.20;
	} else {
		menuBoxWidth = screenWidth * 0.20;
		menuBoxLeft = (screenWidth - menuBoxWidth) / 2;
		menuBoxTop = screenHeight/2 - 150;
	}
	
	buttonLeft = menuBoxWidth * 0.10;
	buttonWidth = menuBoxWidth * 0.80;
	buttonHeight = 40.0;
	
	buttonSpace = 15;
	buttonTop = buttonHeight + buttonSpace;
	
	buttonCount = 5;
	
	menuBoxHeight = (buttonHeight + buttonSpace) * (buttonCount); 
}

function OnEnable () {
	if (clickToMoveScript != null) {
		clickToMoveScript.enabled = false;
	}
}

function OnDisable () {
	if (clickToMoveScript != null) {
		clickToMoveScript.enabled = true;
	}
}

function OnGUI () {
	GUI.skin = MenuSkin;
	
	var windowRect : Rect = Rect (menuBoxLeft, menuBoxTop, menuBoxWidth, menuBoxHeight);
	windowRect = GUILayout.Window (0, windowRect, MainMenuWindow, "Main Menu", GUILayout.ExpandHeight(false));	
	
}

function MainMenuWindow(WindowId : int) {
	/*
	// if "current" game instance exists, allow user to Continue
	if (GUI.Button(new Rect(20,40,80,20), "Continue")) {
		
		// Load last known level
		Application.LoadLevel (1);
	}
	*/	
	
	ShowButton("New Game", NewGameClick, enableStartButton);
	ShowButton("Continue", ContinueClick, enableContinueButton);
	ShowButton("Save Game", SaveClick, enableSaveButton);
	ShowButton("Load Game", LoadClick, enableLoadButton);
	ShowButton("Exit", ExitClick, enableExitButton);
	
}

function ShowButton(buttonLabel : String, callback : Function, enableButton : boolean) {
	GUILayout.Space(buttonSpace);
	GUILayout.BeginHorizontal();
	GUILayout.FlexibleSpace(); 
	if (!enableButton) {
		GUI.enabled = false;
	}
	if (GUILayout.Button(buttonLabel, GUILayout.Width(buttonWidth))) {
		callback();
		
	}
	GUI.enabled = true;
	GUILayout.FlexibleSpace();
	GUILayout.EndHorizontal();
}

function NewGameClick() {
	// Create new instance of game in db
		
	// Load first level
	Application.LoadLevel (1);
}

function ContinueClick() {
}

function SaveClick() {
}

function LoadClick() {
}

function ExitClick() {
	Application.Quit();
}