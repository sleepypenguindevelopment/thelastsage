﻿#pragma strict

public var wardrobe : GameObject = null;

private var player : GameObject = null;
private var mainGuiScript : InGameGui = null;
private var ioScript : InteractiveObject = null;
private var wardrobeScript : InteractiveObject = null;

function Start () {
	player = GameObject.FindWithTag("Player");
	mainGuiScript = player.GetComponent(InGameGui);
	ioScript = GetComponent(InteractiveObject);
	wardrobeScript = wardrobe.GetComponent(InteractiveObject);
}

function Update () {
	if (ioScript.GetCurrentState() == 1) {
		mainGuiScript.showInventoryButton = true;
		wardrobeScript.SetCurrentState(0);
		this.gameObject.SetActive(false);
	}
}