﻿using UnityEngine;
using System.Collections;

public class ActivateInventory : MonoBehaviour {

	public GameObject wardrobe = null;
	
	private GameObject player = null;
	private InGameGui mainGuiScript = null;
	private InteractiveObject ioScript = null;
	private InteractiveObject wardrobeScript = null;
	
	void Start () {
		player = GameObject.FindWithTag("Player");
		mainGuiScript = player.GetComponent("InGameGui") as InGameGui;
		ioScript = GetComponent("InteractiveObject") as InteractiveObject;
		wardrobeScript = wardrobe.GetComponent("InteractiveObject") as InteractiveObject;
	}
	
	void Update () {
		if (ioScript.GetCurrentState() == 1) {
			mainGuiScript.showInventoryButton = true;
			wardrobeScript.SetCurrentState(0);
			this.gameObject.SetActive(false);
		}
	}
}
