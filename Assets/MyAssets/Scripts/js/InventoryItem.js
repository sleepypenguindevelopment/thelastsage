
public var itemName : String = "";
public var description : String = "";
public var icon : Texture = null;
public var activeIcon : Texture = null;
public var radius : int = 3;
public var player : GameObject = null;

private var inventoryScript : Inventory = null;

function Start () {
	if (player == null) {
		player = GameObject.FindWithTag("Player");
	}
	inventoryScript = player.GetComponent(Inventory);
}

function OnMouseDown() {
	
	if (Vector3.Distance(transform.position, player.transform.position) <= radius + 1) {
		inventoryScript.SendMessage("AddToInventory", this);
		gameObject.SetActive(false);
	}
}

function GetItemName() {
	return itemName;
}

function GetIcon() {
	return icon;
}

function GetActiveIcon() {
	return activeIcon;
}