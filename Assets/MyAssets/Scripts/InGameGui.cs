﻿using UnityEngine;
using System.Collections;

public class InGameGui : MonoBehaviour {

	public Texture inventoryIcon = null;
	public Texture mainMenuIcon = null;
	
	public GUISkin customSkin = null;
	
	public bool  showMainMenuButton = true;
	public bool  showInventoryButton = true;
	public bool  showJournalButton = false;
	
	private int areaWidth = 0;
	private int areaHeight = 0;
	private float areaLeft = 0;
	private int areaTop = 0;
	private string message = null;
	private string trueButton = "";
	
	public float spacer = 0.05f;
	
	private bool  overGui = false;
	private float top = 0;
	private float left = 0;
	private float bottom = 0;
	private float right = 0;
	
	private Rect emptyRect = new Rect(0,0,0,0);
	private Rect mainMenuRect = new Rect(0,0,0,0);
	private Rect inventoryRect = new Rect(0,0,0,0);
	
	private Behaviour clickToMoveScript = null;
	private Behaviour mainMenuScript = null;
	private Behaviour inventoryScript = null;
	
	private Person conversant = null;

	void Start() {
		clickToMoveScript = GetComponent("ClickToMove") as Behaviour;
		mainMenuScript = GetComponent("MainMenu") as Behaviour;
		inventoryScript = GetComponent("InventoryGui") as Behaviour;

		areaWidth = 300;
		areaHeight = 10;
		areaLeft = (Screen.width - areaWidth) / 2;
		areaTop = 20;
		message = null;
		trueButton = "Okay";
		
		spacer = 0.05f;

		top = Screen.height * spacer;
		left = Screen.width * spacer;
		bottom = Screen.height - top;
		right = Screen.width - left;

		emptyRect = new Rect(0,0,0,0);
		mainMenuRect = emptyRect;
		inventoryRect = emptyRect;
		
		if (mainMenuIcon != null) {
			mainMenuRect = new Rect(right - mainMenuIcon.width, top, mainMenuIcon.width, mainMenuIcon.height);
		}
		if (inventoryIcon != null) {
			inventoryRect = new Rect(right - inventoryIcon.width, bottom - inventoryIcon.height, inventoryIcon.width, inventoryIcon.height);
		}
	}
	
	void OnGUI() {
		GUI.skin = customSkin;
		areaWidth = Mathf.RoundToInt(Screen.width * 0.75f);
		areaHeight = 10;
		areaLeft = (Screen.width - areaWidth) / 2;
		Rect windowRect = new Rect(areaLeft, areaTop, areaWidth, areaHeight);
		
		// Main Menu Button
		if (showMainMenuButton && mainMenuRect != emptyRect) {	
			if (GUI.Button(mainMenuRect, mainMenuIcon)) {
				ShowGui(mainMenuScript);
			}
		}	
		
		// Inventory Button
		if (showInventoryButton && inventoryRect != emptyRect) {
			if (GUI.Button(inventoryRect, inventoryIcon)) {
				ShowGui(inventoryScript);
			}
		}
		
		if (!string.IsNullOrEmpty(message)) {
			windowRect = GUILayout.Window (0, windowRect, MessageWindow, "", GUILayout.MaxWidth(areaWidth), GUILayout.ExpandHeight(false));	
		}
		
		if (conversant != null) {
			windowRect = GUILayout.Window (0, windowRect, ConversationWindow, "", GUILayout.MaxWidth(areaWidth), GUILayout.ExpandHeight(true));	
		}
	}
	
	void Update() {
		if (mainMenuRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y)) || 
		    inventoryRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y))) {
			if (!overGui) {
				clickToMoveScript.SendMessage("setGuiActive");
				overGui = true;
			}
		} else if (overGui) {
			clickToMoveScript.SendMessage("clearGuiActive");
			overGui = false;
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
			ShowGui(mainMenuScript);
		}
	}
	
	private void MessageWindow( int WindowId  ) {
		clickToMoveScript.SendMessage("setGuiActive");
		GUILayout.Box(message, GUILayout.ExpandWidth(false));
		GUILayout.Space(15);
		if (GUILayout.Button(trueButton, GUILayout.ExpandWidth(false))) {
			message = null;
			trueButton = null;
			Invoke("ClearGuiActive", 1);
		}
	}
	
	private void ConversationWindow(int WindowId) {
		clickToMoveScript.SendMessage("setGuiActive");
		conversant.ConversationWindow(WindowId);
	}
	
	public void ClearGuiActive() {
		clickToMoveScript.SendMessage("clearGuiActive");
	}
	
	public void SetTrueButton(string myTrueButton){
		trueButton = myTrueButton;
	}
	
	public void ShowMessage(string myMessage) {
		message = myMessage;
	}
	
	public void ShowInteraction(Person person) {
		conversant = person;
	}
	
	public void StopInteraction() {
		conversant = null;
		Invoke("ClearGuiActive", 1);
	}
	
	public void ShowGui(Behaviour guiScript) {
		if (guiScript.enabled) {
			guiScript.enabled = false;
		} else {
			guiScript.enabled = true;
		}
	}
	

}
