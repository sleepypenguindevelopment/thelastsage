﻿using UnityEngine;
using System.Collections;

public class InventoryItem : MonoBehaviour {

	public string itemName = "";
	public string description = "";
	public Texture icon = null;
	public Texture activeIcon = null;
	public int radius = 3;
	public GameObject player = null;
	
	private Inventory inventoryScript = null;
	
	void Start() {
		if (player == null) {
			player = GameObject.FindWithTag("Player");
		}
		inventoryScript = player.GetComponent<Inventory>();
	}
	
	void OnMouseDown() {
		
		if (Vector3.Distance(transform.position, player.transform.position) <= radius + 1) {
			inventoryScript.SendMessage("AddToInventory", this);
			gameObject.SetActive(false);
		}
	}
	
	public string GetItemName() {
		return itemName;
	}
	
	public Texture GetIcon() {
		return icon;
	}
	
	public Texture GetActiveIcon() {
		return activeIcon;
	}
}
