# The Last Sage #

*The Last Sage* is a fantasy adventure game with a sci-fi twist.

The Last Sage was inspired by our lead developer's wife who loves complex games, but does not enjoy the fighting that is so prevalent. We chose point & click as it facilitates complex problem solving, and first person perspective to encourage immersion. Goals include telling a unique story, offering multiple options for solving each challenge, and creating a game that does not require violent solutions. We hope to take the player on an engaging adventure not only of intellect, but also of emotion.

An online demo is available at http://games.sleepypenguin.net/devblog/demo/68/
 
### This is a work in progress. ###

**This repository is not the complete project:** There are assets imported from the Unity Asset Store that are not included in this repository.