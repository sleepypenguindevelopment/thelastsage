﻿#pragma strict

public var menuSkin : GUISkin = null;

private var areaWidth : float = 500f;
private var areaHeight : float = 50f;
private var areaLeft : float = (Screen.width - areaWidth) / 2f;
private var areaTop : float = Screen.height - 20f - areaHeight;

private var inventoryScript : Inventory = null;
private var itemScript : InventoryItem = null;
private var windowRect : Rect = Rect (areaLeft, areaTop, areaWidth, areaHeight);
private var clickToMoveScript : Behaviour = null;

private var overGui : boolean = false;
private var activeItem : int = -1;

function Start () {
	inventoryScript = GetComponent(Inventory);
	clickToMoveScript = GetComponent("ClickToMove") as Behaviour;
}

function Update () {
	if (Input.GetButtonDown("Fire1") && !windowRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y))) {
		var hit : RaycastHit;
    	var ray : Ray = Camera.main.ScreenPointToRay (Input.mousePosition);
        if (Physics.Raycast(ray, hit) && hit.transform.gameObject.GetComponent("InteractiveObject") == null 
        	&& hit.transform.gameObject.GetComponent("Person") == null) {
				activeItem = -1;
				inventoryScript.SetActiveItemId(activeItem); 
		}  
	}
	
	if (windowRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y))) {
		if (!overGui) {
			clickToMoveScript.SendMessage("setGuiActive");
			overGui = true;
		}
	} else if (overGui) {
		clickToMoveScript.SendMessage("clearGuiActive");
		overGui = false;
	}
}

function OnGUI () {
	GUI.skin = menuSkin;
	windowRect = GUILayout.Window (1, windowRect, InventoryWindow, "");		
	
}

function InventoryWindow(WindowId : int) {
	var gridTextures : Texture[] = new Texture[10];
	
	var i = 0;
	while (inventoryScript.SlotIsFull(i)) {
		gridTextures[i] = inventoryScript.GetFromInventory(i++).GetIcon();
	}
	
	if (activeItem >= inventoryScript.GetInventorySize()) {
		activeItem = -1;
	}
	
	if (activeItem != -1) {
		gridTextures[activeItem] = inventoryScript.GetFromInventory(activeItem).GetActiveIcon();
	}
	
	
	activeItem = GUILayout.SelectionGrid(activeItem, gridTextures, 10, GUILayout.Height(50), GUILayout.Width(500));
	inventoryScript.SetActiveItemId(activeItem);
	
	
	//Debug.Log("Id: " + activeItem);
	
	
}
