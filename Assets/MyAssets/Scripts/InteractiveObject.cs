﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractiveObject : MonoBehaviour {

	public GUISkin customSkin = null;
	
	public GameObject player;
	public int radius;
	public int startingState;
	public List<ObjectState> states = new List<ObjectState>();
	
	private int currentState;
	
	private bool  interacting = false;
	private Inventory inventoryScript;
	private InGameGui inGameGuiScript = null;
	
	void Start() {
		currentState = startingState;
		inventoryScript = player.GetComponent("Inventory") as Inventory;
		inGameGuiScript = player.GetComponent("InGameGui") as InGameGui;
		
		InitializeStates();
	}
	
	void OnMouseDown() {
		if (!interacting && Vector3.Distance(transform.position, player.transform.position) <= radius + 1) {
			InteractWithObject();
			interacting = false;
		}
	}
	
	void InitializeStates() {
		foreach(ObjectState state in states) {
			if (state.gatewayItem != null) {
				state.SetGatewayItemPassed(false);
			}
			if (state.gatewayScript != null) {
				state.SetGatewayScriptPassed(false);
			}
		}
	}
	
	IEnumerator InteractWithObject() {
		interacting = true;
		
		if (states[currentState].enterMessage != null && states[currentState].enterMessage != "") {
			ShowMessage(states[currentState].enterMessage, "OK");
		}
		
		// Check whether player has given this object the gatewayItem
		if (!states[currentState].GetGatewayItemPassed()) {
			InventoryItem activeItem = inventoryScript.GetActiveItem();
			if (activeItem != null) {			
				if (activeItem.itemName == states[currentState].gatewayItem.itemName) {
					inventoryScript.DeleteFromInventory(activeItem.itemName);
					states[currentState].SetGatewayItemPassed(true);
				}
			}
		}
		
		// If we've passed the gatewayItem and the gatewayScript, perform the exit actions
		if (states[currentState].GetGatewayItemPassed() && states[currentState].GetGatewayScriptPassed()) {
			
			if (states[currentState].exitMessage != null && states[currentState].exitMessage != "") {
				ShowMessage(states[currentState].exitMessage, "OK");
			}
			
			if (states[currentState].exitItem != null) {
				inventoryScript.AddToInventory(states[currentState].exitItem);
				// Should be a bool that determines whether to give once or multiple times
				states[currentState].exitItem = null;
			}
			
			if (states[currentState].exitAnimationTrigger != null && states[currentState].exitAnimationTrigger != "") {
				Animator animator = GetComponent("Animator") as Animator;
				animator.SetTrigger(states[currentState].exitAnimationTrigger);
			}
			if (states[currentState].targetState != -1) {
				bool  autoSwitchState = states[currentState].autoSwitchState;
				currentState = states[currentState].targetState;
				if (autoSwitchState) {
					InteractWithObject();
				}
			}
		} else {
			if (states[currentState].failureMessage != null && states[currentState].failureMessage != "") {
				ShowMessage(states[currentState].failureMessage, "OK");
			}
		}

		return null;
		
	}
	
	void ShowMessage(string myMessage, string myTrueButton) {
		inGameGuiScript.SendMessage("SetTrueButton", myTrueButton);
		inGameGuiScript.SendMessage("ShowMessage", myMessage);
	}
	
	public int GetCurrentState() {
		return currentState;
	}
	
	public void SetCurrentState(int newState) {
		currentState = newState;
	}

	public class ObjectState {
		public string stateName = null;
		
		public int autoInteractRadius = -1;
		
		public string enterMessage = null;
		
		public InventoryItem gatewayItem = null;
		public string gatewayScript = null;
		
		public string failureMessage = null;
		public string exitMessage = null;
		public InventoryItem exitItem = null;
		public string exitCallback = null;
		public string exitAnimationTrigger = null;
		
		public int targetState = 0;
		public bool  autoSwitchState = false;
		
		private bool gatewayItemPassed = true;
		private bool gatewayScriptPassed = true;

		public void SetGatewayItemPassed(bool passed) {
			gatewayItemPassed = passed;
		}
		
		public bool GetGatewayItemPassed() {
			return gatewayItemPassed;
		}
		
		public void SetGatewayScriptPassed(bool passed) {
			gatewayScriptPassed = passed;
		}
		
		public bool GetGatewayScriptPassed() {
			return gatewayScriptPassed;
		}
	}

}
