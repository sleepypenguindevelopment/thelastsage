using UnityEngine;
using System.Collections;

/// Extension of MouseLook
/// Added "hold mouse button" to allow look and calls to NavMeshAgent to move
/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)

public class ClickToMove : MonoBehaviour {
	
	public float sensitivityX = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public int targetRadius = 2;

	private Vector3 target;
	private bool dragging = false;
	private Vector3 clickLocation;
	private bool walking = false;
	private bool guiActive = false;
	private NavMeshAgent agent = null;

	enum Mode{VR, Touch, Mouse};

	private Mode mode;

	void Start ()
	{
		agent = GetComponent<NavMeshAgent>();
		target = transform.position;
		mode = Mode.VR;
	}

	void Update ()
	{
		Ray ray;
		RaycastHit hit;

		switch (mode) {
		
		case Mode.Touch:
		case Mode.VR:
			if (Input.touchCount > 0 || Input.GetButtonDown ("Fire1")) {
				if (!guiActive) {
					ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
					if (Physics.Raycast(ray, out hit)) {
						target = hit.point;
					}
				}

			}
			break;
		case Mode.Mouse:
		default:
			if (Input.GetButtonDown ("Fire1")) {
				dragging = false;
				clickLocation = Input.mousePosition;
			}
			
			if (Input.GetButtonUp ("Fire1") && !dragging && !guiActive) {
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out hit)) {
					target = hit.point;         
				}
			}   
			break;
		}

		if (walking) {
			if (!audio.isPlaying) {
				audio.Play();
			}
		} else {
			audio.Stop();
		}
	}
	
	void FixedUpdate () {
		
		if (Input.GetMouseButton(0))
		{
			if (Vector3.Distance(Input.mousePosition, clickLocation) > 1) {
				dragging = true;
			}

			transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
		}
 
    	if (Vector3.Distance(transform.position, target) > targetRadius) {
			walking = true;
			agent.SetDestination(target);

		} else {
			agent.Stop ();
			walking = false;
		}

		if (agent.velocity == Vector3.zero) {
			walking = false;
		}
	}
	


	void setGuiActive () 
	{
		guiActive = true;
	}

	void clearGuiActive () 
	{
		guiActive = false;
	}

    
}