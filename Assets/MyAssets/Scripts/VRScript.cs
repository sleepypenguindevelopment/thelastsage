﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VRScript : MonoBehaviour {

	Camera myCamera;
	Text crosshair;
	PointerEventData data;

	// Use this for initialization
	void Start () {
		myCamera = GetComponent<Camera> ();
		crosshair = GetComponentInChildren<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0 || Input.GetMouseButtonDown(0)) {
			InitiateVRUIClick();
		}
	}

	void InitiateVRUIClick () {
		// Initiate raycast from crosshair position
		data = new PointerEventData (EventSystem.current);
		data.position = myCamera.WorldToScreenPoint (crosshair.transform.position);

		// Send raycast
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll (data, results);

		// activate "Click" on UI elements, nearest to farthest
		foreach (RaycastResult result in results) {
			if (BubbleClick(result.gameObject)) {
				break;
			}
		}
	}

	// bubble up through hierarchy until we find an Interactive element
	bool BubbleClick(GameObject uiElement) {
		GameObject clickableElement = null;
		Button buttonScript = uiElement.GetComponentInParent<Button> ();
		if (buttonScript != null) {
			clickableElement = buttonScript.gameObject;
		} else {
			Toggle toggleScript = uiElement.GetComponentInParent<Toggle>();
			if (toggleScript != null) {
				clickableElement = toggleScript.gameObject;
			}
		}

		if (clickableElement != null) {
			ExecuteEvents.Execute(clickableElement, data, ExecuteEvents.pointerClickHandler);
			return true;
		}
		return false;
	}
}
