﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Collections.Generic;

public class Person : MonoBehaviour {
	
	public GameObject player;
	public int radius;
	public string personName;
	public TextAsset conversationFile; 
	public GUISkin customSkin;
	
	public InventoryItem gatewayItem = null;
	public List<InventoryItem> startingInventory = new List<InventoryItem>();
	
	private Conversation conversation = null;
	private Prompt convPrompt = null;
	private List<Response> convResponse = new List<Response>();
	private Inventory inventoryScript = null;
	private InGameGui inGameGuiScript = null;
	
	
	void Start() {
		inventoryScript = player.GetComponent("Inventory") as Inventory;
		inGameGuiScript = player.GetComponent("InGameGui") as InGameGui;
		if (radius == 0) {
			radius = 3;
		}
		LoadConversation();
	}
	
	void OnMouseDown() {
		Interact();
	}
	
	public void Interact() {
		if (Vector3.Distance(transform.position, player.transform.position) <= radius) {
			// I've been clicked and am within my activation radius
			
			// Check whether player has given this person a "receivedItem"
			InventoryItem activeItem = inventoryScript.GetActiveItem();
			
			string start = conversation.start;
			foreach (AlternateStart alternateStart in conversation.alternateStarts.Values) {
				if (activeItem != null && activeItem.itemName == alternateStart.receivedItem) {
					inventoryScript.DeleteFromInventory(activeItem.itemName);
					start = alternateStart.start;
				} else if (!string.IsNullOrEmpty(alternateStart.interactionCompleted)) {
					Interaction interaction = conversation.interaction[alternateStart.interactionCompleted] as Interaction;
					if (interaction.completed) {
						start = alternateStart.start;
					}
				}
			}
			ShowInteraction(start);
		}
		
	}
	
	public void ConversationWindow(int WindowId) {
		GUIStyle style = new GUIStyle();
		style.wordWrap = true;
		GUILayout.Box(convPrompt.text, style);
		GUILayout.Space(15);
		foreach (Response response in convResponse) {
			if (GUILayout.Button(response.text, GUILayout.ExpandWidth(false))) {
				if (!string.IsNullOrEmpty(response.requiredInvItem)) {
					inventoryScript.DeleteFromInventory(response.requiredInvItem);
				}
				if (!string.IsNullOrEmpty(response.giveItem)) {
					foreach (InventoryItem item in startingInventory) {
						if (item.itemName == response.giveItem) {
							inventoryScript.AddToInventory(item);
						}
					}
				};
				inGameGuiScript.SendMessage("StopInteraction");
				ShowInteraction(response.target);
			}
		}
	}
	
	private void ShowInteraction(string target) {
		if (target != "end") {
			Interaction interaction = conversation.interaction[target] as Interaction;
			convPrompt = interaction.prompt;
			convResponse = new List<Response>();
			foreach (KeyValuePair<string, Response> response in interaction.response) {
				if (!string.IsNullOrEmpty(response.Value.requiredInvItem)) {
					if (inventoryScript.IsInInventory(response.Value.requiredInvItem)) {
						convResponse.Add(response.Value);
					}
				} else if (!string.IsNullOrEmpty(response.Value.missingInvItem)) {
					if (!inventoryScript.IsInInventory(response.Value.missingInvItem)) {
						convResponse.Add(response.Value);
					}
				} else {
					convResponse.Add(response.Value);
				}
			}
			conversation.interaction[target].completed = true;
			inGameGuiScript.SendMessage("ShowInteraction", this);
		} else {
			conversation.completed = true;
			
			// hack for quick exit
			if (this.name == "Exit") {
				Application.LoadLevel(0);
			}
			
		}
	}
	
	private void LoadConversation() {
		string iid = "";
		string context = "";
		string cid = "";
		string start = "";
		string asid = "";
		string rid = "";
		string target = "";
		Interaction interaction = null;
		if (conversationFile != null) {
			XmlTextReader reader = new XmlTextReader(new StringReader(conversationFile.text));
			while (reader.Read()) {
				if (reader.IsStartElement()) {
					switch (reader.Name) {
					case "conversation":
						context = "conversation";
						cid = reader.GetAttribute("id");
						start = reader.GetAttribute("start");
						conversation = new Conversation(cid, start);
						break;
					case "alternateStart":
						context = "alternateStart";
						asid = reader.GetAttribute("id");
						conversation.alternateStarts.Add(asid, new AlternateStart(asid));
						conversation.alternateStarts[asid].start = reader.GetAttribute("start");
						break;
					case "interactionCompleted":
						if (context == "alternateStart") {
							reader.Read();
							conversation.alternateStarts[asid].interactionCompleted = reader.Value;
						}
						break;
					case "receivedItem":
						if (context == "alternateStart") {
							reader.Read();
							conversation.alternateStarts[asid].receivedItem = reader.Value;
						}
						break;
					case "interaction":
						context = "interaction";
						iid = reader.GetAttribute("id");
						conversation.interaction.Add(iid, new Interaction(iid));
						break;
					case "prompt":
						context = "prompt";
						string pid = reader.GetAttribute("id");
						interaction = conversation.interaction[iid];
						interaction.prompt = new Prompt(pid);
						break;
					case "response":	
						context = "response";		
						rid = reader.GetAttribute("id");
						target = reader.GetAttribute("target");
						interaction = conversation.interaction[iid];
						interaction.response[rid] = new Response(rid, target);
						interaction.response[rid].requiredInvItem = reader.GetAttribute("requiredInvItem");
						interaction.response[rid].missingInvItem = reader.GetAttribute("missingInvItem");
						interaction.response[rid].giveItem = reader.GetAttribute("giveItem");
						break;
					case "text":
						reader.Read();
						string text = reader.Value;
						switch (context) {
						case "prompt":
							interaction = conversation.interaction[iid];
							interaction.prompt.text = text; 
							break;
						case "response":
							interaction = conversation.interaction[iid];
							Response response = interaction.response[rid];
							response.text = text;
							break;
						}
						break;
					}
				}
			}
		}
	}


	private class Conversation {
		public string id = "";
		public string start = "";
		public bool completed = false;
		public Dictionary<string, AlternateStart> alternateStarts = new Dictionary<string, AlternateStart>();
		public Dictionary<string, Interaction> interaction = new Dictionary<string, Interaction>();
		public Conversation(string id, string start) {
			this.id = id;
			this.start = start;
		}
	}

	private class AlternateStart {
		public string id = "";
		public string interactionCompleted = "";
		public string receivedItem = "";
		public string start = "";
		public AlternateStart(string id) {
			this.id = id;
		}
	}

	private class Interaction {
		public string id = "";
		public Prompt prompt = null;
		public Dictionary<string, Response> response = new Dictionary<string, Response>();
		public bool completed = false;
		public Interaction(string id) {
			this.id = id;
		}
	}

	private class Prompt {
		public string id = "";
		public string text = "";
		public string audioClip = "";
		public Prompt(string id) {
			this.id = id;
		}
	}

	private class Response {
		public string id = "";
		public string target = "";
		public string text = "";
		public string requiredInvItem = "";
		public string missingInvItem = "";
		public string giveItem = "";
		public string audioClip = "";
		public Response(string id, string target) {
			this.id = id;
			this.target = target;
		}
	}

}

