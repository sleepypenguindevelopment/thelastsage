﻿#pragma strict

private var player : GameObject = null;
private var inventoryScript : Inventory = null;
private var personScript : Person = null;

function Start() {
	player = GameObject.FindWithTag("Player");
	inventoryScript = player.GetComponent(Inventory);
	personScript = GetComponent(Person);
}

function OnTriggerEnter(other : Collider) {
	var exitItem : String = "Amulet";
	if (inventoryScript.IsInInventory(exitItem)) {
		player.GetComponent(NavMeshAgent).enabled = false;
		personScript.Interact();
	}
}