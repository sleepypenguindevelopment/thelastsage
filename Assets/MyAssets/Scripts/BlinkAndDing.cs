﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlinkAndDing : MonoBehaviour {

	public Camera mainCamera;
	public Text mainText;

	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
		mainText.text = "Height: " + Screen.height + "Width: " + Screen.width;
		if (Input.touchCount > 0 || Input.GetButtonDown ("Fire1")) {
			if (!mainCamera.audio.isPlaying) {
				mainCamera.audio.Play();
			}
			if (mainCamera.backgroundColor == Color.red) {
				mainCamera.backgroundColor = Color.blue;
			} else {
				mainCamera.backgroundColor = Color.red;
			}
		}
	}
}
