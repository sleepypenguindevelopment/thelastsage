#pragma strict

public var inventoryIcon : Texture = null;
public var mainMenuIcon : Texture = null;

public var customSkin : GUISkin = null;

public var showMainMenuButton : boolean = true;
public var showInventoryButton : boolean = true;
public var showJournalButton : boolean = false;

private var areaWidth : int = 300;
private var areaHeight : int = 10;
private var areaLeft : float = (Screen.width - areaWidth) / 2;
private var areaTop : int = 20;
private var message : String = null;
private var trueButton : String = "Okay";

public var spacer : float = 0.05f;

private var overGui : boolean = false;
private var top : float = Screen.height * spacer;
private var left : float = Screen.width * spacer;
private var bottom : float = Screen.height - top;
private var right : float = Screen.width - left;

private var emptyRect : Rect = Rect(0,0,0,0);
private var mainMenuRect : Rect = emptyRect;
private var inventoryRect : Rect = emptyRect;

private var clickToMoveScript : Behaviour = null;
private var mainMenuScript : Behaviour = null;
private var inventoryScript : Behaviour = null;

private var conversant : Person = null;


function Start() {
	clickToMoveScript = GetComponent("ClickToMove") as Behaviour;
	mainMenuScript = GetComponent("MainMenu") as Behaviour;
	inventoryScript = GetComponent("InventoryGui") as Behaviour;

	if (mainMenuIcon != null) {
		mainMenuRect = Rect(right - mainMenuIcon.width, top, mainMenuIcon.width, mainMenuIcon.height);
	}
	if (inventoryIcon != null) {
		inventoryRect = Rect(right - inventoryIcon.width, bottom - inventoryIcon.height, inventoryIcon.width, inventoryIcon.height);
	}
}

function OnGUI () {
	GUI.skin = customSkin;
	areaWidth = Screen.width * 0.75;
	areaHeight = 10;
	areaLeft = (Screen.width - areaWidth) / 2;
	var windowRect : Rect = Rect (areaLeft, areaTop, areaWidth, areaHeight);

	// Main Menu Button
	if (showMainMenuButton && mainMenuRect != emptyRect) {	
		if (GUI.Button(mainMenuRect, mainMenuIcon)) {
			ShowGui(mainMenuScript);
		}
	}	
	
	// Inventory Button
	if (showInventoryButton && inventoryRect != emptyRect) {
		if (GUI.Button(inventoryRect, inventoryIcon)) {
			ShowGui(inventoryScript);
		}
	}
	
	if (!String.IsNullOrEmpty(message)) {
		windowRect = GUILayout.Window (0, windowRect, MessageWindow, "", GUILayout.MaxWidth(areaWidth), GUILayout.ExpandHeight(false));	
	}
	
	if (conversant != null) {
		windowRect = GUILayout.Window (0, windowRect, ConversationWindow, "", GUILayout.MaxWidth(areaWidth), GUILayout.ExpandHeight(true));	
	}
}

function Update () {
	if (mainMenuRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y)) || 
		inventoryRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y))) {
		if (!overGui) {
			clickToMoveScript.SendMessage("setGuiActive");
			overGui = true;
		}
	} else if (overGui) {
		clickToMoveScript.SendMessage("clearGuiActive");
		overGui = false;
	}
	if (Input.GetKeyDown(KeyCode.Escape)) {
		ShowGui(mainMenuScript);
	}
}

function MessageWindow(WindowId : int) {
	clickToMoveScript.SendMessage("setGuiActive");
	GUILayout.Box(message, GUILayout.ExpandWidth(false));
	GUILayout.Space(15);
	if (GUILayout.Button(trueButton, GUILayout.ExpandWidth(false))) {
		message = null;
		trueButton = null;
		Invoke("ClearGuiActive", 1);
	}
}

function ConversationWindow(WindowId : int) {
	clickToMoveScript.SendMessage("setGuiActive");
	conversant.ConversationWindow(WindowId);
}

function ClearGuiActive() {
	clickToMoveScript.SendMessage("clearGuiActive");
}

function SetTrueButton(myTrueButton : String) {
	trueButton = myTrueButton;
}

function ShowMessage(myMessage : String) {
	message = myMessage;
}

function ShowInteraction(person : Person) {
	conversant = person;
}

function StopInteraction() {
	conversant = null;
	Invoke("ClearGuiActive", 1);
}

function ShowGui(guiScript : Behaviour) {
	if (guiScript.enabled) {
		guiScript.enabled = false;
	} else {
		guiScript.enabled = true;
	}
}


