﻿using UnityEngine;
using System.Collections;

public class MainMenuOld : MonoBehaviour {

	public GUISkin MenuSkin = null;
	
	public bool  inGame = false;
	
	// @TODO: calculate whether to show these or make them public?
	private bool enableStartButton = true;
	private bool enableContinueButton = false;
	private bool enableSaveButton = false;
	private bool enableLoadButton = false;
	private bool enableExitButton = true;
	
	private float screenWidth = Screen.width;
	private float screenHeight = Screen.height;
	
	// determine which buttons should display, then calculate menu box height
	private float menuBoxLeft;
	private float menuBoxTop;
	private float menuBoxWidth;

	private float buttonWidth;
	private float buttonHeight;
	
	private float buttonSpace;
	
	private int buttonCount;
	
	private float menuBoxHeight; 
	
	private Behaviour clickToMoveScript = null;

	private delegate void ClickCallback();
	
	// @TODO: replace with 2D array of Menu Title and Function to call
	// ex: buttons["startButton"] = {"New Game", "StartNewGame"}
	
	void Start() {

		clickToMoveScript = GetComponent("ClickToMove") as Behaviour;
		
		if (clickToMoveScript != null) {
			clickToMoveScript.enabled = false;
		}
		
		if (inGame) {
			menuBoxLeft = screenWidth * 0.20f;
			menuBoxTop = screenHeight * 0.20f;
			menuBoxWidth = screenWidth * 0.20f;
		} else {
			menuBoxWidth = screenWidth * 0.20f;
			menuBoxLeft = (screenWidth - menuBoxWidth) / 2;
			menuBoxTop = screenHeight/2 - 150;
		}

		buttonWidth = menuBoxWidth * 0.80f;
		buttonHeight = 40.0f;
		
		buttonSpace = 15;
		
		buttonCount = 5;
		
		menuBoxHeight = (buttonHeight + buttonSpace) * (buttonCount); 
	}
	
	void OnEnable() {
		if (clickToMoveScript != null) {
			clickToMoveScript.enabled = false;
		}
	}
	
	void OnDisable() {
		if (clickToMoveScript != null) {
			clickToMoveScript.enabled = true;
		}
	}
	
	void OnGUI() {
		GUI.skin = MenuSkin;
		
		Rect windowRect = new Rect(menuBoxLeft, menuBoxTop, menuBoxWidth, menuBoxHeight);
		windowRect = GUILayout.Window (0, windowRect, MainMenuWindow, "Main Menu", GUILayout.ExpandHeight(false));	
	}
	
	void MainMenuWindow (int WindowId ) {
		/*
	// if "current" game instance exists, allow user to Continue
	if (GUI.Button(new Rect(20,40,80,20), "Continue")) {
		
		// Load last known level
		Application.LoadLevel (1);
	}
	*/	

		ClickCallback newGameClick = NewGameClick;
		ClickCallback continueClick = ContinueClick;
		ClickCallback saveClick = SaveClick;
		ClickCallback loadClick = LoadClick;
		ClickCallback exitClick = ExitClick;

		ShowButton("New Game", newGameClick, enableStartButton);
		ShowButton("Continue", continueClick, enableContinueButton);
		ShowButton("Save Game", saveClick, enableSaveButton);
		ShowButton("Load Game", loadClick, enableLoadButton);
		ShowButton("Exit", exitClick, enableExitButton);
		
	}
	
	void ShowButton(string buttonLabel, ClickCallback callback, bool enableButton){
		GUILayout.Space(buttonSpace);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace(); 
		if (!enableButton) {
			GUI.enabled = false;
		}
		if (GUILayout.Button(buttonLabel, GUILayout.Width(buttonWidth))) {
			callback();	
		}
		GUI.enabled = true;
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}



	private void NewGameClick() {
		// Create new instance of game in db
		
		// Load first level
		Application.LoadLevel (1);
	}
	
	void ContinueClick() {
	}
	
	void SaveClick() {
	}
	
	void LoadClick() {
	}
	
	void ExitClick() {
		Application.Quit();
	}
}
