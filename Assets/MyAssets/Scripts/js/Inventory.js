// startingInventory is public so items can be added via the Unity Editor
public var startingInventory : List.<InventoryItem> = new List.<InventoryItem>();

private var inventory : List.<InventoryItem> = new List.<InventoryItem>();
private var activeItemId : int = -1;

function Awake () {
	inventory = startingInventory;
}

function IsInInventory (itemName : String) : boolean {
	for (var item : InventoryItem in inventory) {	
		if (item.itemName == itemName) {
			return true;
		}
	}
	return false;
}

function GetInventorySize() : int {
	return inventory.Count;
}

function SlotIsFull(itemId : int) : boolean {
	if (itemId < inventory.Count && inventory[itemId] != null) {
		return true;
	}
	return false;
}

function AddToInventory (item : InventoryItem) {
	inventory.Add(item);
}

function GetFromInventory (itemId : int) : InventoryItem {
	return inventory[itemId];
}

function GetActiveItem() {
	if (activeItemId != -1) {
		return inventory[activeItemId];
	}
	return null;
}

function DeleteFromInventory (itemId : int) : boolean {
	SetActiveItemId(-1);
	return inventory.Remove(inventory[itemId]);
}

function DeleteFromInventory (itemName : String ) : boolean {
	return DeleteFromInventory(GetIdFromName(itemName));
}

function GetIdFromName(itemName : String) {
	var i = 0;
	for (var item : InventoryItem in inventory) {
		if (item.itemName == itemName) {
			return i;
		}
		i++;
	}
	return -1;
}

function SetActiveItemId(itemId : int) {
	activeItemId = itemId;
}

function GetActiveItemId() : int {
	return activeItemId;
}


