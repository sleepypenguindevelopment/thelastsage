﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public GameObject player;
	public CanvasRenderer mainMenuPanel;
	public CanvasRenderer displayModePanel;
	public Camera mainCamera;
	public Camera leftCamera;
	public Camera rightCamera;
	 
	OpenDiveSensor diveScript;
	ClickToLook clickToLookScript;
	Canvas canvas;

	// Use this for initialization
	void Start () {

		diveScript = player.GetComponent<OpenDiveSensor> ();
		clickToLookScript = player.GetComponentInChildren<ClickToLook> ();
		canvas = GetComponent<Canvas>();

		displayModePanel.gameObject.SetActive (false);
		SwipeToLookClick ();
		// @TODO: if there is a saved game, activate Continue button
	}

	// Main Menu functions
	public void ContinueClick() {
	}

	public void NewGameClick() {
		// @TODO: Create new instance of game in db
		
		// Load first level
		Application.LoadLevel (1);
	}

	public void SettingsClick() {
		displayModePanel.gameObject.SetActive (true);
		mainMenuPanel.gameObject.SetActive (false);

	}

	public void ExitClick() {
		Debug.Log ("Goodbye!");
		Application.Quit();
	}

	// Display Mode Menu functions
	public void SwipeToLookClick() {
		// activate main camera

		mainCamera.gameObject.SetActive (true);
		leftCamera.gameObject.SetActive (false);
		rightCamera.gameObject.SetActive (false);

		canvas.worldCamera = mainCamera;
		diveScript.cameraleft = mainCamera;
		diveScript.cameraright = mainCamera;
		diveScript.enabled = false;
		clickToLookScript.enabled = true;
	}

	public void PartialVRClick() {
		// activate main camera
		mainCamera.gameObject.SetActive (true);
		leftCamera.gameObject.SetActive (false);
		rightCamera.gameObject.SetActive (false);

		canvas.worldCamera = mainCamera;
		diveScript.cameraleft = mainCamera;
		diveScript.cameraright = mainCamera;
		diveScript.enabled = true;
		clickToLookScript.enabled = false;
	}
	
	public void FullVRClick() {
		// activate left & right cameras
		mainCamera.gameObject.SetActive (false);
		leftCamera.gameObject.SetActive (true);
		rightCamera.gameObject.SetActive (true);

		canvas.worldCamera = leftCamera;
		diveScript.cameraleft = leftCamera;
		diveScript.cameraright = rightCamera;
		diveScript.enabled = true;
		clickToLookScript.enabled = false;
	}

	public void DisplayBackClick() {
		mainMenuPanel.gameObject.SetActive (true);
		displayModePanel.gameObject.SetActive (false);
	}
}
