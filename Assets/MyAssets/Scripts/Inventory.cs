﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	// startingInventory is public so items can be added via the Unity Editor
	public List<InventoryItem> startingInventory = new List<InventoryItem>();
	
	private List<InventoryItem> inventory = new List<InventoryItem>();
	private int activeItemId = -1;
	
	void Awake() {
		inventory = startingInventory;
	}
	
	public bool IsInInventory(string itemName) {
		foreach(InventoryItem item in inventory) {	
			if (item.itemName == itemName) {
				return true;
			}
		}
		return false;
	}
	
	public int GetInventorySize() {
		return inventory.Count;
	}
	
	public bool SlotIsFull(int itemId) {
		if (itemId < inventory.Count && inventory[itemId] != null) {
			return true;
		}
		return false;
	}
	
	public void  AddToInventory(InventoryItem item) {
		inventory.Add(item);
	}
	
	public InventoryItem GetFromInventory(int itemId) {
		return inventory[itemId];
	}

	public InventoryItem GetFromInventory(string itemName) {
			return GetFromInventory (GetIdFromName (itemName));
	}
	
	public InventoryItem GetActiveItem() {
		if (activeItemId != -1) {
			return inventory[activeItemId];
		}
		return null;
	}
	
	public bool DeleteFromInventory(int itemId) {
		SetActiveItemId(-1);
		return inventory.Remove(inventory[itemId]);
	}
	
	public bool DeleteFromInventory(string itemName) {
		return DeleteFromInventory(GetIdFromName(itemName));
	}
	
	public int GetIdFromName(string itemName) {
		int i = 0;
		foreach(InventoryItem item in inventory) {
			if (item.itemName == itemName) {
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public void SetActiveItemId(int itemId){
		activeItemId = itemId;
	}
	
	public int GetActiveItemId() {
		return activeItemId;
	}

}
