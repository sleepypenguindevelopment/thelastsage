#pragma strict

import System.Xml;
import System.IO;
import System.Collections.Generic;

public var player : GameObject;
public var radius : int;
public var personName : String;
public var conversationFile : TextAsset; 
public var customSkin : GUISkin;

public var gatewayItem : InventoryItem = null;
public var startingInventory : List.<InventoryItem> = new List.<InventoryItem>();

private var conversation : Conversation = null;
private var convPrompt : Prompt = null;
private var convResponse : List.<Response> = new List.<Response>();
private var inventoryScript : Inventory = null;
private var inGameGuiScript : InGameGui = null;


function Start() {
	inventoryScript = player.GetComponent(Inventory);
	inGameGuiScript = player.GetComponent(InGameGui);
	if (!radius) {
			radius = 3;
	}
	LoadConversation();
}

function OnMouseDown() {
	Interact();
}

function Interact() {
	if (Vector3.Distance(transform.position, player.transform.position) <= radius) {
		// I've been clicked and am within my activation radius
		
		// Check whether player has given this person a "receivedItem"
		var activeItem : InventoryItem = inventoryScript.GetActiveItem();
	
		var start = conversation.start;
		for (var alternateStart : AlternateStart in conversation.alternateStarts.Values) {
			if (activeItem != null && activeItem.itemName == alternateStart.receivedItem) {
					inventoryScript.DeleteFromInventory(activeItem.itemName);
					start = alternateStart.start;
			} else if (!String.IsNullOrEmpty(alternateStart.interactionCompleted)) {
				var interaction : Interaction = conversation.interaction[alternateStart.interactionCompleted] as Interaction;
				if (interaction.completed) {
					start = alternateStart.start;
				}
			}
		}
		ShowInteraction(start);
	}
	
}

function ConversationWindow(WindowId : int) {
	var style = new GUIStyle();
	style.wordWrap = true;
	GUILayout.Box(convPrompt.text, style);
	GUILayout.Space(15);
	for (var response : Response in convResponse) {
		if (GUILayout.Button(response.text, GUILayout.ExpandWidth(false))) {
			if (!String.IsNullOrEmpty(response.requiredInvItem)) {
				inventoryScript.DeleteFromInventory(response.requiredInvItem);
			}
			if (!String.IsNullOrEmpty(response.giveItem)) {
				for (var item : InventoryItem in startingInventory) {
					if (item.itemName == response.giveItem) {
						inventoryScript.AddToInventory(item);
					}
				}
			};
			inGameGuiScript.SendMessage("StopInteraction");
			ShowInteraction(response.target);
		}
	}
}

function ShowInteraction(target : String) {;
	if (target != "end") {
		var interaction : Interaction = conversation.interaction[target] as Interaction;
		convPrompt = interaction.prompt;
		convResponse = new List.<Response>();
		for (var response in interaction.response) {
			if (!String.IsNullOrEmpty(response.Value.requiredInvItem)) {
				if (inventoryScript.IsInInventory(response.Value.requiredInvItem)) {
					convResponse.Add(response.Value);
				}
			} else if (!String.IsNullOrEmpty(response.Value.missingInvItem)) {
				if (!inventoryScript.IsInInventory(response.Value.missingInvItem)) {
					convResponse.Add(response.Value);
				}
			} else {
				convResponse.Add(response.Value);
			}
		}
		conversation.interaction[target].completed = true;
		inGameGuiScript.SendMessage("ShowInteraction", this);
	} else {
		conversation.completed = true;
		
		// hack for quick exit
		if (this.name == "Exit") {
			Application.LoadLevel(0);
		}
		
	}
}

function LoadConversation() {
	var iid : String = "";
	var context : String = "";
	var cid : String = "";
	var start : String = "";
	var interaction : Interaction = null;
	if (conversationFile != null) {
		var reader : XmlTextReader = new XmlTextReader(new StringReader(conversationFile.text));
		while (reader.Read()) {
			if (reader.IsStartElement()) {
				switch (reader.Name) {
					case "conversation":
						context = "conversation";
						cid = reader.GetAttribute('id');
						start = reader.GetAttribute('start');
						conversation = new Conversation(cid, start);
						break;
					case "alternateStart":
						context = "alternateStart";
						var asid = reader.GetAttribute('id');
						conversation.alternateStarts.Add(asid, new AlternateStart(asid));
						conversation.alternateStarts[asid].start = reader.GetAttribute('start');
						break;
					case "interactionCompleted":
						if (context == "alternateStart") {
							reader.Read();
							conversation.alternateStarts[asid].interactionCompleted = reader.Value;
						}
						break;
					case "receivedItem":
						if (context == "alternateStart") {
							reader.Read();
							conversation.alternateStarts[asid].receivedItem = reader.Value;
						}
						break;
					case "interaction":
						context = "interaction";
						iid = reader.GetAttribute('id');
						conversation.interaction.Add(iid, new Interaction(iid));
						break;
					case "prompt":
						context = "prompt";
						var pid = reader.GetAttribute('id');
						interaction = conversation.interaction[iid];
						interaction.prompt = new Prompt(pid);
						break;
					case "response":	
						context = "response";		
						var rid = reader.GetAttribute('id');
						var requiredInvItem = reader.GetAttribute('requiredInvItem');
						var target = reader.GetAttribute('target');
						interaction = conversation.interaction[iid];
						interaction.response[rid] = new Response(rid, target);
						interaction.response[rid].requiredInvItem = reader.GetAttribute('requiredInvItem');
						interaction.response[rid].missingInvItem = reader.GetAttribute('missingInvItem');
						interaction.response[rid].giveItem = reader.GetAttribute('giveItem');
						break;
					case "text":
						reader.Read();
						var text = reader.Value;
						switch (context) {
							case "prompt":
								interaction = conversation.interaction[iid];
								interaction.prompt.text = text; 
								break;
							case "response":
								interaction = conversation.interaction[iid];
								var response : Response = interaction.response[rid];
								response.text = text;
								break;
						}
				}
			}
		}
	}
}

class Conversation {
	var id : String = "";
	var start : String = "";
	var completed : boolean = false;
	var alternateStarts : Dictionary.<String, AlternateStart> = new Dictionary.<String, AlternateStart>();
	var interaction : Dictionary.<String, Interaction> = new Dictionary.<String, Interaction>();
	function Conversation(id : String, start : String) {
		this.id = id;
		this.start = start;
	}
}

class AlternateStart {
	var id : String = "";
	var interactionCompleted : String = "";
	var receivedItem : String = "";
	var start : String = "";
	function AlternateStart(id : String) {
		this.id = id;
	}
}

class Interaction {
	var id : String = "";
	var prompt : Prompt = null;
	var response : Dictionary.<String, Response> = new Dictionary.<String, Response>();
	var completed : boolean = false;
	function Interaction(id : String) {
		this.id = id;
	}
}

class Prompt {
	var id = "";
	var text = "";
	var audioClip = "";
	function Prompt(id : String) {
		this.id = id;
	}
}

class Response {
	var id = "";
	var target = "";
	var text = "";
	var requiredInvItem = "";
	var missingInvItem = "";
	var giveItem = "";
	var audioClip = "";
	function Response(id : String, target : String) {
		this.id = id;
		this.target = target;
	}
}