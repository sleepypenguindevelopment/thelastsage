﻿using UnityEngine;
using System.Collections;

public class ExitScript : MonoBehaviour {

	private GameObject player = null;
	private Inventory inventoryScript = null;
	private Person personScript = null;
	
	void Start() {
		player = GameObject.FindWithTag("Player");
		inventoryScript = player.GetComponent<Inventory>();
		personScript = GetComponent<Person>();
	}
	
	void OnTriggerEnter( Collider other  ) {
		string exitItem = "Amulet";
		if (inventoryScript.IsInInventory(exitItem)) {
			player.GetComponent<NavMeshAgent>().enabled = false;
			personScript.Interact();
		}
	}
}
