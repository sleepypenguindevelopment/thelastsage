﻿using UnityEngine;
using System.Collections;

public class InventoryGui : MonoBehaviour {

	public GUISkin menuSkin = null;
	
	private float areaWidth = 0;
	private float areaHeight = 0;
	private float areaLeft = 0;
	private float areaTop = 0;
	
	private Inventory inventoryScript = null;
	private Rect windowRect = new Rect(0,0,0,0);
	private Behaviour clickToMoveScript = null;
	
	private bool  overGui = false;
	private int activeItem = -1;
	
	void Start() {
		inventoryScript = GetComponent("Inventory") as Inventory;
		clickToMoveScript = GetComponent("ClickToMove") as Behaviour;

		areaWidth = 500f;
		areaHeight = 50f;
		areaLeft = (Screen.width - areaWidth) / 2f;
		areaTop = Screen.height - 20f - areaHeight;
		windowRect = new Rect(areaLeft, areaTop, areaWidth, areaHeight);
		
	}
	
	void Update() {
		if (Input.GetButtonDown("Fire1") && !windowRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y))) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast(ray, out hit) && hit.transform.gameObject.GetComponent("InteractiveObject") == null 
			    && hit.transform.gameObject.GetComponent("Person") == null) {
				activeItem = -1;
				inventoryScript.SetActiveItemId(activeItem); 
			}  
		}
		
		if (windowRect.Contains(new Vector2(Input.mousePosition.x,Screen.height - Input.mousePosition.y))) {
			if (!overGui) {
				clickToMoveScript.SendMessage("setGuiActive");
				overGui = true;
			}
		} else if (overGui) {
			clickToMoveScript.SendMessage("clearGuiActive");
			overGui = false;
		}
	}
	
	void OnGUI() {
		GUI.skin = menuSkin;
		windowRect = GUILayout.Window (1, windowRect, InventoryWindow, "");		
	}
	
	void InventoryWindow(int WindowId) {
		Texture[] gridTextures = new Texture[10];
		
		int i = 0;
		while (inventoryScript.SlotIsFull(i)) {
			gridTextures[i] = inventoryScript.GetFromInventory(i++).GetIcon();
		}
		
		if (activeItem >= inventoryScript.GetInventorySize()) {
			activeItem = -1;
		}
		
		if (activeItem != -1) {
			gridTextures[activeItem] = inventoryScript.GetFromInventory(activeItem).GetActiveIcon();
		}

		activeItem = GUILayout.SelectionGrid(activeItem, gridTextures, 10, GUILayout.Height(50), GUILayout.Width(500));
		inventoryScript.SetActiveItemId(activeItem);
	}

}
